# Contributing

## Clone and compile

Clone the [QMK Firmware](https://github.com/qmk/qmk_firmware) and follow the [environment setup](https://docs.qmk.fm/#/newbs_getting_started?id=environment-setup).

Then clone this project into the `keyboards/planck/keymaps` folder:

```bash
cd keyboards/planck/keymaps
git clone https://gitlab.com/brezel/public/bepo/planck-bepo.git

```

If environment is correctly configured, you can build the firmware with:

```bash
make planck/rev6:planck-bepo

```
